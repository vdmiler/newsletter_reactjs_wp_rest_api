export const requestUrl = 'http://rest.websters.ru';

export const menuItems = [
   {
      path: '/',
      label: 'Главная'
   },
   {
      path: '/news',
      label: 'Новости'
   },
   {
      path: '/contacts',
      label: 'Контакты'
   },
]
