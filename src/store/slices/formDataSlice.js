import axios from 'axios';
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { requestUrl } from '../../components/constans';

export const fetchFormData = createAsyncThunk(
   'news/fetchFormData',
   async function (info, { rejectWithValue }) {
      try {
         const response = await axios({
            method: 'post',
            url: requestUrl + '/wp-json/wp/v2/feedback',
            data: {
               title: info.formData.name,
               fields: {
                  email: info.formData.email,
               },
               content: info.formData.message,
               status: 'publish'
            },
            headers: {
               'Content-Type': 'application/json',
               'accept': 'application/json',
               'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9yZXN0LndlYnN0ZXJzLnJ1IiwiaWF0IjoxNjM2NTY3MzU4LCJuYmYiOjE2MzY1NjczNTgsImV4cCI6MTYzNzE3MjE1OCwiZGF0YSI6eyJ1c2VyIjp7ImlkIjoiMiJ9fX0.olz0yp8CrcgRcYDS-vMn0RqBL-AcPexk8bP_rDXEzrY'
            }
         })
         return response.data
      } catch (error) {
         return rejectWithValue(error.message)
      }
   }
);

const initialState = {
   formData: {},
   loadingForm: false,
   errorForm: null,
}

const formDataSlice = createSlice({
   name: 'formData',
   initialState,
   extraReducers: {
      [fetchFormData.pending]: (state, action) => {
         state.loadingForm = true;
      },
      [fetchFormData.fulfilled]: (state, action) => {
         state.loadingForm = false;
         state.formData = action.payload;
      },
      [fetchFormData.rejected]: (state, action) => {
         state.loadingForm = false;
         state.errorForm = action.payload;
      },
   }
});

export const { addFormData } = formDataSlice.actions;

export default formDataSlice.reducer;